# coding: utf-8

from django.contrib.gis.db import models

from niamoto_taxa.models import Taxon


class Occurrence(models.Model):
    """
    Model representing a plant occurrence, in it's simplest form, i.e.
    a unique identifier, a geographic location, a taxon (if identified),
    """

    id = models.CharField(primary_key=True, max_length=20)
    date = models.DateField(null=True, blank=True)
    taxon = models.ForeignKey(Taxon, null=True, blank=True)
    location = models.PointField(srid=4326, null=True, blank=True)
