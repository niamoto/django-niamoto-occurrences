=====
django-niamoto-occurrences
=====

Django reusable application providing models and utilities related to plant
occurrences in the niamoto system.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "niamoto_occurrences" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'niamoto_occurrences',
    ]

2. Run `python manage.py migrate` to create the niamoto_occurrences models.

